Dockerfile standard
===================

..or what <https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/syntax.md>
might become with some help.

[[_TOC_]]

Support status
--------------

### kaniko

[![pipeline status](https://gitlab.com/iguillaumed/playground/dockerfile-standard/badges/kaniko/pipeline.svg?ignore_skipped=true&key_text=pipeline+status)](https://gitlab.com/iguillaumed/playground/dockerfile-standard/-/commits/kaniko)

See !1 and the outcome of the CI pipelines there.

### buildah

[![pipeline status](https://gitlab.com/iguillaumed/playground/dockerfile-standard/badges/buildah/pipeline.svg?ignore_skipped=true&key_text=pipeline+status)](https://gitlab.com/iguillaumed/playground/dockerfile-standard/-/commits/buildah)

TODO
